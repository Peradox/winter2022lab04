import java.util.Scanner;
public class Shop1{
	public static void main(String[] args){
		Album1[] albums= new Album1[4];
		Scanner scan= new Scanner(System.in);
		String albumName;
		String artist;
		String format;
		
		for(int i=0;i<albums.length;i++)
		{
			System.out.println(" ");
			System.out.println("Album #"+(i+1));
			System.out.println("What is the album name?");
			albumName=scan.nextLine();
			System.out.println("Who is the artist?");
			artist=scan.nextLine();
			System.out.println("What format is it in? e.g. cassette");
			format=scan.nextLine();
			albums[i]= new Album1(albumName,artist,format);
		}
		System.out.println(" ");
		System.out.println("update Album #4:");
		
		System.out.println("What is the album name?");
		albumName=scan.nextLine();
		System.out.println("Who is the artist?");
		artist=scan.nextLine();
		System.out.println("What format is it in? e.g. cassette");
		format=scan.nextLine();
		
		System.out.println("Album name: "+albums[3].getAlbumName());
		System.out.println("Artist: "+albums[3].getArtist());
		System.out.println("Format: "+albums[3].getFormat());
		
		albums[3]= new Album1("","",format);
		albums[3].setAlbumName(albumName);
		albums[3].setArtist(artist);
		
		System.out.println("Album name: "+albums[3].getAlbumName());
		System.out.println("Artist: "+albums[3].getArtist());
		System.out.println("Format: "+albums[3].getFormat());
		
		albums[3].musicPlayer();
		
	}
	
}