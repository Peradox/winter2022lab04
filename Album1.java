public class Album1{
	private String name;
	private String artist;
	private String format;
	
	public Album1(String name, String artist, String format){
		this.name="Album name";
		this.artist="Artist name";
		this.format="Format";
		
		this.name=name;
		this.artist=artist;
		this.format=format;
	}
	
	public void setAlbumName(String name){
		this.name=name;
	}
	public void setArtist(String artist){
		this.artist=artist;
	}
	public String getAlbumName(){
		return this.name;
	}
	public String getArtist(){
		return this.artist;
	}
	public String getFormat(){
		return this.format;
	}
	public void musicPlayer(){
		format=this.format.toLowerCase();
		System.out.println("You will need a "+this.format+" player to play this album");
	}
}